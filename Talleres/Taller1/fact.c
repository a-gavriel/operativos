#include <stdio.h>
#include <stdlib.h>

unsigned long fact_aux(unsigned long n , unsigned long r){
	if(n>1){
		return fact_aux(n-1,r*n);
	}else{
		return r;
	}
}

unsigned long fact(unsigned long n){
	return fact_aux(n,1);
}

int main(int argc, char *argv[]){
	unsigned long x = atoi(argv[1]);
	unsigned long res = fact(x);
	printf("%lu\n",res);

}


