import sys
def fact(n):
  return (fact_aux(n,1))

def fact_aux(n,r):
  if n>1:
    return fact_aux(n-1,r*n)
  else:
    return r

res = fact(int(sys.argv[1]))
print(res)
